//
//  ReportListCollectionViewCell.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 17/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class ReportListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var reportTitle: UILabel!
    @IBOutlet weak var reportDescription: UILabel!
    @IBOutlet weak var reportImage: UIImageView!
    @IBOutlet weak var reportDate: UILabel!
    @IBOutlet weak var reportTags: UICollectionView!
    @IBOutlet weak var reportStatus: UILabel!

    var tags: [String] = []
}

extension ReportListCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let labelWidwth = (tags[indexPath.row] as NSString).size(withAttributes: nil).width
        let size = CGSize.init(width: labelWidwth + 15, height: 20)
        return size
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = reportTags.dequeueReusableCell(withReuseIdentifier: "reportTagCollectionViewCell", for: indexPath) as? ReportTagCollectionViewCell

        cell!.contentView.layer.cornerRadius = 10
        cell!.contentView.layer.masksToBounds = true
        cell!.layer.cornerRadius = 10
        cell!.layer.masksToBounds = true

        cell?.tagLabel.text = tags[indexPath.row]

        return cell!
    }

}
