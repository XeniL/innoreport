//
//  ReportTagCollectionViewCell.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 18/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class ReportTagCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var tagLabel: UILabel!
}

