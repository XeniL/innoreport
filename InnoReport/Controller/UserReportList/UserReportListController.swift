//
//  UserReportListController.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 23/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class UserReportListController: UIViewController {

    @IBOutlet weak var reportCollectionView: UICollectionView!

    private var reports: [Report] = []
    private let refreshControl = UIRefreshControl()


    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkManager.shared.getUserReports(success: getReports(reports:))
        reportCollectionView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    func getReports (reports: [ApiReport]) {
        self.reports = reports.toEntity()
        DispatchQueue.main.async { [weak self] in
            self?.reportCollectionView.reloadData()
        }
    }

    @objc private func refreshData(_ sender: Any) {
        NetworkManager.shared.getUserReports(success: getReports(reports:))
        self.refreshControl.endRefreshing()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ReportDetailViewController
        {
            let detailViewController = segue.destination as? ReportDetailViewController
            let cell = sender as! ReportListCollectionViewCell
            let indexPath = self.reportCollectionView.indexPath(for: cell)
            let report = self.reports[indexPath!.row]
            detailViewController!.reportTitle = report.title!
            detailViewController!.reportDescription = report.description!
            detailViewController!.reportDate = report.date!
            detailViewController!.reportStatus = cell.reportStatus.text!
            detailViewController?.imagePath = report.imagePath!
            detailViewController!.reportTags = report.tags as! [String]
        }
    }
}

extension UserReportListController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = reportCollectionView.dequeueReusableCell(withReuseIdentifier: "reportListCollectionViewCell", for: indexPath) as? ReportListCollectionViewCell

        cell!.contentView.layer.cornerRadius = 10.0
        cell!.contentView.layer.masksToBounds = true
        cell!.layer.cornerRadius = 10.0
        cell!.layer.masksToBounds = true

        cell!.layer.shadowColor = UIColor.black.cgColor
        cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
        cell!.layer.shadowRadius = 5
        cell!.layer.shadowOpacity = 0.45
        cell!.layer.masksToBounds = false
        cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 5.0).cgPath

        cell?.reportTitle.text = reports[indexPath.row].title
        cell?.reportDescription.text = reports[indexPath.row].description
        cell?.reportDate.text = reports[indexPath.row].date
        cell?.reportImage.kf.setImage(with: URL(string: reports[indexPath.row].imagePath!))
        cell?.reportStatus.text = reports[indexPath.row].status

        switch cell?.reportStatus.text {
        case "RECEIVED":
            cell?.reportStatus.text = "Report was recieved"
        case "IN_PROGRESS":
            cell?.reportStatus.text = "Report is being proccesed"
        case "SOLVED":
            cell?.reportStatus.text = "Report was solved"
        case "DECLINED":
            cell?.reportStatus.text = "Report was declined"
        default:
            cell?.reportStatus.text = "Report status unknown"
        }

        cell?.tags = reports[indexPath.row].tags as! [String]

        return cell!
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reports.count
    }
}
