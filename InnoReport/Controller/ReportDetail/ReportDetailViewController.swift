//
//  ReportDetailViewController.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 23/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit
import Kingfisher

class ReportDetailViewController: UIViewController {

    @IBOutlet weak var reportImage: UIImageView!
    @IBOutlet weak var reportTitleLabel: UILabel!
    @IBOutlet weak var reportDescriptionLabel: UITextView!
    @IBOutlet weak var reportStatusLabel: UILabel!
    @IBOutlet weak var reportDateLabel: UILabel!
    @IBOutlet weak var reportTagsCollection: UICollectionView!

    var reportTitle: String = ""
    var reportDescription: String = ""
    var reportDate: String = ""
    var reportStatus:String = ""
    var reportTags: [String] = []
    var imagePath: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        reportTitleLabel.text = reportTitle
        reportDescriptionLabel.text = reportDescription
        reportDateLabel.text = reportDate
        reportStatusLabel.text = reportStatus
        reportImage.kf.setImage(with: URL(string: imagePath))
    }
}

extension ReportDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reportTags.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let labelWidwth = (reportTags[indexPath.row] as NSString).size(withAttributes: nil).width
        let size = CGSize.init(width: labelWidwth + 15, height: 20)
        return size
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = reportTagsCollection.dequeueReusableCell(withReuseIdentifier: "reportTagCollectionViewCell", for: indexPath) as? ReportTagCollectionViewCell

        cell!.contentView.layer.cornerRadius = 10
        cell!.contentView.layer.masksToBounds = true
        cell!.layer.cornerRadius = 10
        cell!.layer.masksToBounds = true

        cell?.tagLabel.text = reportTags[indexPath.row]

        return cell!
    }


}
