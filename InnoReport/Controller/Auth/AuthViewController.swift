//
//  AuthViewController.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 16/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class AuthViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        logoImage.layer.cornerRadius = 50
        logoImage.layer.borderWidth = 3
        logoImage.layer.borderColor = UIColor.white.cgColor

        submitButton.isEnabled = false
        submitButton.setTitleColor(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), for: .disabled)
        submitButton.layer.borderWidth = 3
        submitButton.layer.borderColor = UIColor.white.cgColor

        self.passTextField.delegate = self
        self.emailTextField.delegate = self

        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        self.hideKeyboardWhenTappedAround()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func authSuccessful() {
        stopAnimating()
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "isLoggedIn")
        performSegue(withIdentifier: "auth", sender: self)
    }

    func authFailure(error: String) {
        stopAnimating()
        showAlert(title: "Error", message: "Password or email was incorrect")
    }
    
    @IBAction func submitName(_ sender: Any) {
        startAnimating()
        NetworkManager.shared.authenticate(login: emailTextField.text!, password: passTextField.text!, success: authSuccessful, failure: authFailure(error:))
    }

    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension AuthViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passTextField.becomeFirstResponder()
        default:
            passTextField.resignFirstResponder()
        }
        return true
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if isValidEmail(testStr: emailTextField.text!) == true && passTextField.text!.count > 0 {
            submitButton.isEnabled = true
        } else {
            submitButton.isEnabled = false
        }
    }

}

