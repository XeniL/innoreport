//
//  SubmitReportViewController.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 18/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SubmitReportViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var reportTitle: UITextField!
    @IBOutlet weak var reportDescription: UITextView!
    @IBOutlet weak var reportDate: UITextField!
    @IBOutlet weak var reportTags: UICollectionView!
    @IBOutlet weak var reportImage: UIImageView!
    @IBOutlet weak var tagLabel: UILabel!

    private var datePicker: UIDatePicker?
    private let imagePicker = UIImagePickerController()
    private let 😂👌: String = "Provide description of your problem"
    private let date = Date()
    private let dateFormatter = DateFormatter()
    private var tags: [String] = []
    static var selectedTags: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()

        self.hideKeyboardWhenTappedAround()

        let dateTapGesture = UITapGestureRecognizer(target: self, action: #selector(SubmitReportViewController.viewTapped(gestureRecogniser:)))

        let reportImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(SubmitReportViewController.uploadImagePressed))

        let reportTagsTapGesture = UITapGestureRecognizer(target: self, action: #selector(SubmitReportViewController.tagsListTapped(gestureRecogniser:)))

        reportTags.addGestureRecognizer(reportTagsTapGesture)
        reportImage.addGestureRecognizer(reportImageTapGesture)
        view.addGestureRecognizer(dateTapGesture)
    }

    override func viewDidAppear(_ animated: Bool) {
        imagePicker.delegate = self
        reportTitle.delegate = self
        reportDescription.delegate = self
        reportDate.delegate = self

        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.locale = Locale(identifier: "ru")
        datePicker?.addTarget(self, action: #selector(SubmitReportViewController.dateChanged(datePicker:)), for: .valueChanged)
        datePicker!.maximumDate = date

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(SubmitReportViewController.viewTapped(gestureRecogniser:)))

        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()


        reportDate.inputView = datePicker
        reportDate.inputAccessoryView = toolBar
    }

    override func viewWillAppear(_ animated: Bool) {
        tagsSelected(tags: SubmitReportViewController.selectedTags)
    }

    override func viewWillDisappear(_ animated: Bool) {
        SubmitReportViewController.selectedTags = []
    }

    func tagsSelected(tags: [String]){
        if tags.count > 0 {
            tagLabel.isHidden = true
        } else {
            tagLabel.isHidden = false
        }
        self.tags = tags
        reportTags.reloadData()
    }

    func setUpUI() {
        reportDescription.text = 😂👌
        reportDescription.textColor = .lightGray

        reportImage.layer.cornerRadius = 8
        reportImage.clipsToBounds = true

        reportDescription.layer.borderWidth = 0.5
        reportDescription.layer.borderColor = UIColor.lightGray.cgColor
        reportDescription.layer.masksToBounds = true
        reportDescription.layer.cornerRadius = 8

        dateFormatter.dateFormat = "dd/MM/yyyy"
        reportDate.text = dateFormatter.string(from: date)
    }

    @objc func dateChanged(datePicker: UIDatePicker) {
        reportDate.text = dateFormatter.string(from: datePicker.date)
    }

    @objc func viewTapped(gestureRecogniser: UITapGestureRecognizer){
        view.endEditing(true)
    }

    @objc func tagsListTapped(gestureRecogniser: UITapGestureRecognizer) {
        let viewController:TagsTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "tagsViewController") as! TagsTableViewController

        self.present(viewController, animated: true, completion: nil)
    }

    func dataSentSuccsesfully(){
        stopAnimating()
        self.navigationController?.popToRootViewController(animated: true)
    }

    func showError(error: String) {
        stopAnimating()
        showAlert(title: "Something went wrong", message: error)
    }

    @IBAction func submitReport(_ sender: Any) {
        if reportTitle.text != "" && reportDescription.text != "" && tags.count > 0 {
            startAnimating()
            let imageData = reportImage.image?.jpegData(compressionQuality: 0.5)
            NetworkManager.shared.postReport(title: reportTitle.text!, description: reportDescription.text, date: reportDate.text!, location: "50 50", tags: tags, image: imageData!, success: dataSentSuccsesfully, failure: showError(error:))
        } else {
            stopAnimating()
            showAlert(title: "Not all fields are filled", message: "Please provide a complete information about your problem")
        }
    }
}

extension SubmitReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func showImagePickerActionSheet() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)

       optionMenu.addAction(UIAlertAction(title: "Take a photo", style: .default, handler: { (UIAlertAction) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))

        optionMenu.addAction(UIAlertAction(title: "Choose from library", style: .default , handler: { (UIAlertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))

        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction)in
            return
        }))

        self.present(optionMenu, animated: true, completion: nil)
    }

    @objc func uploadImagePressed() {
        showImagePickerActionSheet()
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        let image = info[.originalImage] as? UIImage
        let imgData = NSData(data: (info[.originalImage] as! UIImage).jpegData(compressionQuality: 1)!)
        let imageSize: Int = imgData.count
        let imageSizeInMb = Double(imageSize) / 1024.0/1024.0

        print("actual size of image in mB: %f ", Double(imageSize) / 1024.0/1024.0)

        if imageSizeInMb > 5.0 {
            dismiss(animated: true, completion: nil)
            showAlert(title: "Image size is too big", message: "Image should not exceed 5 Mb in size")

        } else {
            reportImage.contentMode = .scaleAspectFill
            reportImage.image = image
            dismiss(animated: true, completion: nil)
        }

    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
    
}

extension SubmitReportViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let labelWidwth = (tags[indexPath.row] as NSString).size(withAttributes: nil).width
        let size = CGSize.init(width: labelWidwth + 15, height: 20)

        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = reportTags.dequeueReusableCell(withReuseIdentifier: "reportTagCollectionViewCell", for: indexPath) as? ReportTagCollectionViewCell

        cell!.contentView.layer.cornerRadius = 10
        cell!.contentView.layer.masksToBounds = true
        cell!.layer.cornerRadius = 10
        cell!.layer.masksToBounds = true

        cell?.tagLabel.text = tags[indexPath.row]

        return cell!
    }

}

extension SubmitReportViewController: UITextFieldDelegate, UITextViewDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case reportTitle:
            reportDescription.becomeFirstResponder()
        default:
            reportDescription.resignFirstResponder()
        }
        return true
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count

        if text == "\n" {
            reportDate.becomeFirstResponder()
            return false
        }
        return numberOfChars < 600
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text as! NSString).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.count

        return numberOfChars < 50
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == "") {
            self.reportDescription.text = 😂👌
            self.reportDescription.textColor = .lightGray
        }
    }

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.textColor = .black

        if(textView.text == 😂👌) {
            textView.text = ""
        }

        return true
    }
}
