//
//  TagsTableViewController.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 21/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class TagsTableViewController: UIViewController {

    @IBOutlet weak var tagsTableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    private let tags: [String] = ["DIRT",
                                  "ELECTRICITY",
                                  "INTERNET",
                                  "DORM",
                                  "ROAD"]
    var selectedTags: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tagsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")

        setNavigationBar()
    }

    func setNavigationBar() {
        let navItem = UINavigationItem(title: "")
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(completeSelection))
        let backItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissSelection(_:)) )

        navItem.rightBarButtonItem = doneItem
        navItem.leftBarButtonItem = backItem
        navigationBar.setItems([navItem], animated: false)
    }

    @objc func dismissSelection (_ sender: Any) {
        SubmitReportViewController.selectedTags = SubmitReportViewController.selectedTags
        dismiss(animated: true, completion: nil)
    }

    @objc func completeSelection (_ sender: Any) {
        SubmitReportViewController.selectedTags = selectedTags
        dismiss(animated: true, completion: nil)
    }

}

extension TagsTableViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tags.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        cell.textLabel?.text = tags[indexPath.row]


        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tagsTableView.cellForRow(at: indexPath)?.accessoryType == .checkmark {
            tagsTableView.cellForRow(at: indexPath)?.accessoryType = .none
            selectedTags.removeAll() { $0 == tagsTableView.cellForRow(at: indexPath)?.textLabel!.text! }
        } else {
            tagsTableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            selectedTags.append((tagsTableView.cellForRow(at: indexPath)?.textLabel!.text)!)
        }

        tagsTableView.deselectRow(at: indexPath, animated: true)
    }
}
