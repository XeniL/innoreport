//
//  Report.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 23/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation

struct Report {
    let title : String?
    let description: String?
    let location: String?
    let date: String?
    let imagePath: String?
    let status: String?
    let tags: [String?]
}
