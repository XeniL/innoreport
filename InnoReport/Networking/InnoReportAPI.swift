//
//  InnoReportAPI.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 17/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Moya

enum InnoReportAPI {
    case authentification(login: String, password: String)
    case getReports
    case getUserReports
    case sendReport(imageData: Data, title: String, description: String, date: String, location: String, tags: [String])
}

enum Server {
    case report
    case auth

    var baseUrl: URL {
        switch self {
        case .report:
            return URL(string: "http://10.90.138.222:8080")!
        case .auth:
            return URL(string: "http://10.90.138.222:5002")!
        }
    }
}

extension InnoReportAPI: TargetType {
    public var baseURL: URL {
        switch self {
        case .authentification(_):
            return Server.auth.baseUrl
        default:
            return Server.report.baseUrl
        }
    }

    public var path: String {
        switch self {
        case .getUserReports:
            return "/reports/userReports/"
        case .authentification(_):
            return "/innoreports/login"
        case .sendReport(_):
            return "/reports/create"
        case .getReports:
            return "/reports"
        }
    }

    public var method: Method {
        switch self {
        case .authentification(_), .sendReport(_):
            return .post
        case .getReports, .getUserReports:
            return .get
        }
    }

    public var sampleData: Data {
        return Data()
    }

    public var parameters: [String: Any] {
        switch self {
        case .sendReport(_, let title, let description, let date, let location, let tags):
            return ["title": title, "description":description, "date":date, "tags":tags, "location":location]
        case .authentification(let login, let password):
            return ["login": login, "password": password]
        case .getReports, .getUserReports:
            return [String: Any]()
        }
    }

    public var parameterEncoding: ParameterEncoding {
        switch self {
        case .authentification(_), .getUserReports:
            return URLEncoding.default
        default: return JSONEncoding.default
        }
    }

    public var task: Task {
        switch self {
        case .sendReport(let imageData, _, _, _, _, _):
            let jpegData = MultipartFormData(provider: .data(imageData), name: "image", fileName: "picture.jpeg", mimeType: "image/jpeg")
            let multipartData = [jpegData]
            return .uploadCompositeMultipart(multipartData, urlParameters: parameters)

        default: return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        }
    }

    public var headers: [String: String]? {
        switch self {
        case .getUserReports, .sendReport(_):
            let defaults = UserDefaults.standard
            return ["Authorization": "Bearer \(defaults.value(forKey: "userToken") as! String)", "UserEmail": defaults.value(forKey: "userEmail") as! String]
        default:
            return [String: String]()
        }
    }
}
