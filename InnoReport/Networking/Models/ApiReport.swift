//
//  ApiReport.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 21/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation

struct ApiReport: Decodable {
    let rId: String?
    let title : String?
    let description: String?
//    let location: String?
    let date: String?
    let imagePath: String?
    let status: String?
    let tags: [String?]

    enum ReportCodingKeys: String, CodingKey {
        case rId = "rId"
        case title = "title"
        case description = "description"
//        case location = "location"
        case date = "date"
        case imagePath = "imagePath"
        case status = "status"
        case tags = "tags"
    }

init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: ReportCodingKeys.self)
    rId = try container.decode(String.self, forKey: .rId)
    title = try container.decode(String.self, forKey: .title)
    description = try container.decode(String.self, forKey: .description)
//    location = try container.decode(String.self, forKey: .location)
    date = try container.decode(String.self, forKey: .date)
    imagePath = try container.decode(String.self, forKey: .imagePath)
    status = try container.decode(String.self, forKey: .status)
    tags = try container.decode([String].self, forKey: .tags)
    }

}

extension Collection where Element == ApiReport {
    func toEntity() -> [Report] {
        return self.map {

            let report = Report(title: $0.title, description: $0.description, location: "", date: $0.date, imagePath: $0.imagePath, status: $0.status, tags: $0.tags)

            return report
        }
    }
}
