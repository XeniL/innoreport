//
//  NetworkManager.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 17/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Moya
import SwiftyJSON
import Alamofire

struct NetworkManager: InnoReportAPIProtocol {
    internal var provider: MoyaProvider<InnoReportAPI>
    static let shared = NetworkManager()

    init() {
        let configuration = URLSessionConfiguration.default
        var plugin = [PluginType]()
        let manager = Manager(configuration: configuration)

        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        configuration.requestCachePolicy = .useProtocolCachePolicy

        plugin = [NetworkLoggerPlugin(verbose: true)]
        provider = MoyaProvider<InnoReportAPI>(manager: manager, plugins: plugin)
    }

    func postReport(title: String, description: String, date: String, location:String, tags: [String], image: Data, success: @escaping () -> Void, failure: @escaping (String) -> Void) {
        provider.request(.sendReport(imageData: image, title: title, description: description, date: date, location: location, tags: tags)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let filteredResponse = try moyaResponse.filterSuccessfulStatusCodes()
                    success()
                } catch let error {
                    failure(error.localizedDescription)
                    print(error.localizedDescription)
                }
            case let .failure(error):
                failure(error.localizedDescription)
                print(error.localizedDescription)
            }
        }
    }

    func getReports(success: @escaping ([ApiReport]) -> Void) {
        provider.request(.getReports) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let filteredResponse = try moyaResponse.filterSuccessfulStatusCodes()
                    let results = try JSONDecoder().decode([ApiReport].self, from: filteredResponse.data)
                    success(results)
                } catch let error {
                    print(error.localizedDescription)
                }
            case let .failure(error):
                print (error.localizedDescription)
            }
        }
    }

    func getUserReports(success: @escaping ([ApiReport]) -> Void) {
        provider.request(.getUserReports) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let filteredResponse = try moyaResponse.filterSuccessfulStatusCodes()
                    let results = try JSONDecoder().decode([ApiReport].self, from: filteredResponse.data)
                    success(results)
                } catch let error {
                    print(error.localizedDescription)
                }
            case let .failure(error):
                print (error.localizedDescription)
            }
        }
    }

    func authenticate(login: String, password: String, success: @escaping () -> Void, failure: @escaping (String) -> Void) {
        provider.request(.authentification(login: login, password: password)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let filteredResponse = try moyaResponse.filterSuccessfulStatusCodes()
                    let data = filteredResponse.data
                    let json = try JSON(data: data)

                    let defaults = UserDefaults.standard
                    let token = json["token"].string
                    let email = json["email"].string

                    defaults.set(email, forKey: "userEmail")
                    defaults.set(token, forKey: "userToken")

                    success()
                } catch let error {
                    failure(error.localizedDescription)
                }
            case let .failure(error):
                failure(error.localizedDescription)
            }
        }
    }

    

}
