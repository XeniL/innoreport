//
//  InnoReportAPIProtocol.swift
//  InnoReport
//
//  Created by Nikita Elizarov on 17/04/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Moya

protocol InnoReportAPIProtocol {
    var provider: MoyaProvider<InnoReportAPI> { get }
    func getReports(success: @escaping ([ApiReport]) -> Void)
    func postReport(title: String, description: String, date: String, location: String ,tags: [String], image: Data, success: @escaping () -> Void, failure: @escaping (String) -> Void)
    func getUserReports(success: @escaping ([ApiReport]) -> Void)
    func authenticate(login: String, password: String, success: @escaping () -> Void, failure: @escaping (String) -> Void)
}
